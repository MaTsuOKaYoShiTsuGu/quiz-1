package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class GameIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    private AnnotationConfigApplicationContext context;
    @Test
    void should_create_successful_when_post_uri() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/games"))
                .andExpect(MockMvcResultMatchers.status().is(201));
    }

    @Test
    void should_get_correct_game_url() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/games"))
                .andExpect(MockMvcResultMatchers.header().string("Location","/api/games/1"));
    }

    @Test
    void should_get_game_status_when_get_uri() throws Exception {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        Game game= context.getBean(Game.class);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/games/1"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.content().string("{ \"id\":"+game.getGameId()+", \"answer\": \""+game.getAnswer()+"+\"}"));
    }

    @Test
    void should_return_not_found_when_get_not_exist_game_id() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/games/23333"))
                .andExpect(MockMvcResultMatchers.status().is(404));

    }

    ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void should_return_result_when_guess_answer() throws Exception {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        Game game= context.getBean(Game.class);
        Answer answer = new Answer("9876");
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/games/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(answer)))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.content().string("{\"hint\":\""+game.getHint(answer.getAnswer())+"\",\"correct\":"+game.isCorrect(answer.getAnswer())+"}"));
    }
    @Test
    void should_return_correct_result_when_guess_answer_success() throws Exception {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        Game game= context.getBean(Game.class);
        Answer answer = new Answer(game.getAnswer());
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/games/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(answer)))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.content().string("{\"hint\":\""+game.getHint(answer.getAnswer())+"\",\"correct\":"+game.isCorrect(answer.getAnswer())+"}"));
    }

    @Test
    void should_return_404_when_game_id_is_not_exist() throws Exception {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        Game game= context.getBean(Game.class);
        Answer answer = new Answer(game.getAnswer());
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/games/2333")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(answer)))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    void should_return_400_when_game_id_is_not_number() throws Exception {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        Game game= context.getBean(Game.class);
        Answer answer = new Answer(game.getAnswer());
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/games/game")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(answer)))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_return_400_when_answer_is_not_4_digits() throws Exception {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        Game game= context.getBean(Game.class);
        Answer answer = new Answer("15555");
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/games/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(answer)))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_return_400_when_answer_contain_other_symbol_except_digit() throws Exception {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
        Game game= context.getBean(Game.class);
        Answer answer = new Answer("s");
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/games/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(answer)))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }
}
