package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
public class GameController {

    @PostMapping("/api/games")
    ResponseEntity createGame(){
        Game game = new GameConstructHelper().createGame();
        return ResponseEntity.status(HttpStatus.CREATED)
                .header("Location",
                        "/api/games/"+String.valueOf(game.getGameId()))
                .body("");
    }

    @GetMapping("/api/games/{gameId}")
    ResponseEntity getGame(@PathVariable int gameId){
        Map<Integer,Game> gameMap = GameConstructHelper.getGameMap();
        if(!gameMap.containsKey(gameId)){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("");
        }
        Game game = gameMap.get(gameId);
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body("{ \"id\":"+game.getGameId()+", \"answer\": \""+game.getAnswer()+"+\"}");
    }

    @PatchMapping("/api/games/{gameId}")
    ResponseEntity getGameStatus(@PathVariable int gameId,@RequestBody @Valid Answer answer){
        Map<Integer,Game> gameMap = GameConstructHelper.getGameMap();
        if(!gameMap.containsKey(gameId)){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("");
        }
        if(!answer.AnswerFormatJudge()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("");
        }
        Game game = gameMap.get(gameId);
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body("{\"hint\":\""+game.getHint(answer.getAnswer())+"\",\"correct\":"+game.isCorrect(answer.getAnswer())+"}");
    }
}
