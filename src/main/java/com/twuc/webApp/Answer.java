package com.twuc.webApp;

import java.util.HashSet;
import java.util.Set;

public class Answer {

    private String answer;

    public Answer(String answer) {
        this.answer = answer;
    }

    public String getAnswer() {
        return answer;
    }

    public Answer() {
    }

    public boolean AnswerFormatJudge(){
        Set<Character> charSet = new HashSet<>();
        for(int i=0;i<answer.length();i++)
            charSet.add(answer.charAt(i));
        return answer.length()==4&&charSet.size()==4;
    }
}
