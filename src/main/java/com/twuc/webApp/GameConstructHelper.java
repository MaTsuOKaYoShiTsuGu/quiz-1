package com.twuc.webApp;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.*;

@Configuration
public class GameConstructHelper {

    private static Map<Integer, Game> gameMap = new HashMap<Integer, Game>();
    private int gameId;

    @Bean
    public Game createGame(){
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        while(sb.length()<4){
            int digit = random.nextInt(10);
            if(sb.indexOf(String.valueOf(digit))<0)
                sb.append(digit);
        }
        Game game = new Game(++gameId,sb.toString());
        gameMap.put(gameId,game);
        return game;
    }

    public static Map<Integer, Game> getGameMap() {
        return gameMap;
    }
}
